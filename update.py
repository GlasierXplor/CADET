#This script is responsible for the ClamAV Databases to be in the right state to be used, i.e. permissible, present, up to date, and extracted.
from __future__ import print_function
import urllib
import conf
import hashlib
import os
import tarfile
import time
import verifyfiles
import logger

#Decide what to do.
#If main.cvd is present, check for updates.
#If main.cvd not present, download and unpack needed files.
#Will always verify the downloaded file (md5 hash is in header)
def verify(version):
    if version == "NEW":
        cvd = "./cvd/main.cvd"
        gzip = "./cvd/main.cvd.tar.gz"
        md5 = "./cvd/md5.hash"

    elif version == "UPDATE":
        cvdold = "./cvd/main.cvd"
        gzipold = "./cvd/main.cvd.tar.gz"
        md5old = "./cvd/md5.hash"
        cvd = "./cvd/main.cvd.new"
        gzip = "./cvd/main.cvd.tar.gz.new"
        md5 = "./cvd/md5.hash.new"

    elif version == "FORCE":
        cvdold = "./cvd/main.cvd"
        gzipold = "./cvd/main.cvd.tar.gz"
        md5 = "./cvd/md5.hash"
        cvd = "./cvd/main.cvd.new"
        gzip = "./cvd/main.cvd.tar.gz.new"

    logger.logger("INFO", "Verifying archive...", "database")

    with open(cvd, 'rb') as mainfile:
        with open(gzip, 'wb') as gzipfile:
            gzipfile.write(mainfile.read()[512:])
            gzipfile.close()
        mainfile.close()

    with open(cvd, 'rb') as mainfile:
        fileinfo = mainfile.read()[0:512]
        mainfile.close()
    fileinfo = fileinfo.split(':')

    BLOCKSIZE = 65536
    hasher = hashlib.md5()
    with open(gzip, 'rb') as gzipfile:
        buf = gzipfile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = gzipfile.read(BLOCKSIZE)
    md5hash = hasher.hexdigest()

    if fileinfo[5] == md5hash:
        logger.logger("INFO", "Verified!", "database")
        with open(md5, 'w') as md5file:
            md5file.write(fileinfo[5])
            md5file.close()
        if version == "NEW":
            return True
#NEW DOWNLOAD ENDS HERE

    else:
        logger.logger("ERROR", "Hash sum mismatch. Re-fetching main.cvd...", "database")
        return False

    if version == "UPDATE":
        with open(md5old, 'r') as file1:
            with open(md5, 'r') as file2:
                if file1.readlines()[0] == file2.readlines()[0]:
                    file1.close()
                    file2.close()
                    logger.logger("INFO", "CVD up to date.", "database")
                    os.remove(cvd)
                    os.remove(gzip)
                    os.remove(md5)
                    return False
                else:
                    file1.close()
                    file2.close()
                    os.remove(cvdold)
                    os.remove(gzipold)
                    os.remove(md5old)
                    os.rename(cvd, cvdold)
                    os.rename(gzip, gzipold)
                    os.rename(md5, md5old)
                    gzip = gzipold
                    for files in conf.files_needed:
                        if os.path.isfile(files):
                            try:
                                rmfile = os.path.join('./cvd', files)
                                os.remove(rmfile)
                            except:
                                logger.logger("ERROR", "I cannot remove " + files + ". Check file permissions?", "database")
                        else:
                            pass
                    return True

    elif version == "FORCE":
        os.remove(cvdold)
        os.remove(gzipold)
        os.rename(cvd, cvdold)
        os.rename(gzip, gzipold)
        gzip = gzipold
        for files in conf.files_needed:
            if os.path.isfile(files):
                try:
                    rmfile = os.path.join('./cvd', files)
                    os.remove(rmfile)
                except:
                    logger.logger("ERROR", "I cannot remove " + files + ". Check file permissions?", "database")
            else:
                pass
        return True

#Progress bar for urllib.urlretrieve
def reporthook(count, bs, total):
    global starttime
    if count == 0:
        starttime = time.time()
        return
    duration = time.time() - starttime
    progress = int(count * bs)
    speed = int(progress / (1024 * duration))
    percent = min(int(count * bs * 100 / total), 100)
    print("...%d%%, %dMB/%dMB downloaded @ %dkB/s   " % (percent, progress / (1024 * 1024), total / (1024 * 1024), speed), end='\r')

#verifies uisng the file headers only.
def verifyupdated():
    cvdlink = 'http://database.clamav.net/main.cvd'
    ud_filepath = './cvd/main.cvd.new'

    with open('./cvd/newheader.txt') as headerfile:
        with open('./cvd/md5.hash') as md5hash:
            fileinfo = headerfile.readline().split(':')
            md5 = md5hash.readline()

            if fileinfo[5] == md5:
                logger.logger("INFO", "main.cvd is the most updated version already", "database")
                os.remove('./cvd/newheader.txt')
                return True

            else:
                logger.logger("WARN", "main.cvd is outdated. Downloading new version", "database")
                try:
                    urllib.urlretrieve(cvdlink, ud_filepath, reporthook)
                except:
                    logger.logger("FATAL", "Something may be stopping me from fetching main.cvd. Check your network connectivity or firewalls", "database")
                logger.logger("INFO", "Fetched successfully!", "database")
                os.remove('./cvd/newheader.txt')
                return False

#extracts all files in the list in conf.files_needed, changes their permissions
def extract(extractee):
    gzip = './cvd/main.cvd.tar.gz'
    tar = tarfile.open(gzip, 'r')
    tar.extract(extractee, "./cvd")
    logger.logger("INFO", "" + extractee + " extracted.", "database")
    newfile = os.path.join("./cvd", extractee)
    os.chmod(newfile, 0600)

#Checks for network connectivity
def checknetwork():
    logger.logger("INFO", "Checking network connectivity...", "database")

    try:
        urllib.urlopen('http://example.com')
    except:
        logger.logger("ERROR", "No network connectivity...", "database")
        return False

    logger.logger("INFO", "Success", "database")
    return True

#Depends on task, calls other functions accordingly.
def download(task):
    cvdlink = 'http://database.clamav.net/main.cvd'
    dl_filepath = './cvd/main.cvd'
    fr_filepath = './cvd/main.cvd.new'

#Downloads main.cvd from ClamAV
    if checknetwork():
        try:
            if task == "DOWNLOAD":
                logger.logger("INFO", "Fetching main.cvd from ClamAV...", "database")
                urllib.urlretrieve(cvdlink, dl_filepath, reporthook)
            elif task == "UPDATE":
                logger.logger("INFO", "Fetching main.cvd's file header...", "database")
                with open('./cvd/newheader.txt', 'w') as headerfile:
                    write = urllib.urlopen(cvdlink).read(512)
                    headerfile.write(write)
                headerfile.close()
            elif task == "FORCE":
                logger.logger("INFO", "Fetching main.cvd from ClamAV...", "database")
                urllib.urlretrieve(cvdlink, fr_filepath, reporthook)

        except:
            logger.logger("FATAL", "Something may be stopping me from fetching the databases. Check your network connectivity or firewalls.", "database")
            download(task)

        logger.logger("INFO", "Fetched successfully!", "database")

        if task == "DOWNLOAD":
            count = 0
            while count < 5:
                if verify("NEW"):
                    logger.logger("INFO", "Done. Extracting Databases...", "database")
                    for files in conf.files_needed:
                        extract(files)
                    verifyfiles.verifyfiles()
                    return True
                    break
                else:
                    logger.logger("ERORR: Failed. Trying again... Attempt " + count + "/5.", "database")
                    sleep(3)
                    verify("NEW")
            logger.logger("FATAL", "Cannot extract databases!", "database")

        elif task == "UPDATE":
            updated = verifyupdated()

            if updated:
                logger.logger("INFO", "Checking for needed files...", "database")

                for files in conf.files_needed:
                    filepath = os.path.join('./cvd', files)

                    if not os.path.isfile(filepath):
                        logger.logger("WARN", "" + files + " is not present. Extracting...", "database")
                        extract(files)
                    else:
                        logger.logger("INFO", "File " + files + " present.", "database")

                verifyfiles.verifyfiles()

            else:
                verify("UPDATE")
                logger.logger("INFO", "Extracting needed files...", "database")

                for files in conf.files_needed:
                    extract(files)
                verifyfiles.verifyfiles()
                return True

        elif task == "FORCE":
            count = 0
            while count < 5:
                if verify("FORCE"):
                    logger.logger("INFO", "Done. Extracting Databases...", "database")
                    for files in conf.files_needed:
                        extract(files)
                    verifyfiles.verifyfiles()
                    return True
                else:
                    logger.logger("ERORR: Failed. Trying again... Attempt " + count + "/5.", "database")
                    verify("FORCE")
            logger.logger("FATAL", "Cannot extract databases!", "database")

    else:
        if task == "DOWNLOAD" or task == "FORCE":
            for files in conf.files_needed:
                filepath = os.path.join('./cvd', files)
                if os.path.isfile(filepath):
                    logger.logger("WARN", "main.cvd not found but main.mdb is present", "database")
            logger.logger("ERROR", "main.cvd cannot be downloaded!", "database")

        elif task == "UPDATE":
            failed = 0
            for files in conf.files_needed:
                filepath = os.path.join('./cvd', files)

                if os.path.isfile(filepath):
                    logger.logger("INFO", "main.cvd and main.mdb are present", "database")

                else:
                    logger.logger("INFO", "Cannot find " + files + ". Extracting...", "database")
                    extract(files)
                    verifyfiles.verifyfiles()
                    return
        return

#What runs if update.py is called directly instead of from database.py
if __name__ == "__main__":
    logger.logger("INFO", "On-demand update/download of ClamAV Virus Database.", "database")
    if os.path.isfile("./cvd/main.cvd", "database"):
        download("UPDATE")
    else:
        download("DOWNLOAD")
