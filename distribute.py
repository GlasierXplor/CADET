import socket
from sendfile import sendfile
import conf
import os
import subprocess
from mpi4py import MPI

def __init__():
    comm = MPI.COMM_WORLD
    rank = comm.rank
    size = comm.size

def files():
    try:
        print 'creating database dir'
        cmd = 'pssh -h ' + conf.ipfile + ' -l timothy "echo DesertXplor123 | mkdir /home/timothy/Documents/MP/CADET/databases"'
        subprocess.check_output(cmd, shell=True)

        print 'copying databases'
        for files in conf.sendfiles:
            cmd = 'pscp -h ' + conf.ipfile + ' -l timothy /home/timothy/Documents/MP/CADET' + files + ' /home/timothy/Documents/MP/CADET' + files
            subprocess.check_output(cmd, shell=True)

    except:
        pass

def connectiscsi():
    print 'connecting all nodes to iscsi targets'
    try:
        cmd = '''pssh -h ''' + conf.ipfile + ''' -H 127.0.0.1 -l timothy "echo DesertXplor123 | sudo -S python -c \\"import os; os.chdir('/home/timothy/Documents/MP/CADET/'); import iscsi; iscsi.connectiscsi()\\""'''
        subprocess.check_output(cmd, shell=True)

    except:
        pass

def findluns():
    print 'finding luns to mount'
    try:
        cmd = '''pssh -h "''' + conf.ipfile + '''" -H 127.0.0.1 -l timothy "echo DesertXplor123 | sudo -S python -c \\"import os; os.chdir('/home/timothy/Documents/MP/CADET/'); import iscsi; iscsi.findluns()\\""'''
        subprocess.check_output(cmd, shell=True)

    except:
        pass

def main():

