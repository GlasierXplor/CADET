import hashlib
import conf
import os
import update
import logger

def sha256hash(inputfile):
    BLOCKSIZE = 65536
    hasher = hashlib.sha256()
    with open(inputfile, 'rb') as infile:
        buf = infile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = infile.read(BLOCKSIZE)
    return hasher.hexdigest()

#verifies the hashes against the ones present in main.info
def verifyfiles():
    retry = 0
    #these are fail saves in case for some reason update.py is not spitting out the files.
    if not os.path.isfile('./cvd/main.info'):
        logger.logger("WARN", "main.info not found. Attempting to extract from main.cvd.tar.gz...", "database")
        update.extract("main.info")

    for files in conf.files_needed:
        filepath = os.path.join('./cvd', files)

        if not os.path.isfile(filepath):
            logger.logger("WARN", "" + files + " not found. Attempting to extract from main.cvd.tar.gz...", "database")
            update.extract(files)

    while retry < 4:
#grabs the SHA256 hashes of the individual files
        with open("./cvd/main.info") as infofile:
            count = 0
            filehash = {}
            logger.logger("INFO", "Grabbing file verifier...", "database")
            for i, l in enumerate(infofile):
                pass
            maxcount = i
            infofile.seek(0)
            for line in infofile.readlines():
                line = line.rstrip("\n")
                data = line.split(':')
                if count == 0 or count == maxcount:
                    count += 1
                else:
                    if data[0] in conf.files_needed:
                        filehash[data[0]] = data[2]
                    count += 1
        infofile.close()

        for key in filehash:
            newpath = os.path.join('./cvd', key)
            calchash = sha256hash(newpath)
            if calchash == filehash[key]:
                logger.logger("INFO", "" + key + " verified!", "database")
                retry += 5
            else:
                if retry < 3:
                    logger.logger("WARN", "" + key + " verification failed!", "database")
                    retry += 1
                else:
                    logger.logger("ERROR", "" + key + " verification failed. Re-downloading main.cvd...", "database")
                    update.download("FORCE")
