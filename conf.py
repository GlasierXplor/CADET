#Config file that contains configurations and also global variables that will be used across the programme.
import multiprocessing as mp
import os
import Queue

def findtarget():
    targetlist = []
    if os.stat(databasedir + 'target').st_size == 0:
        print "Targets file not found. Please create the file before proceeding"
        exit()
    with open(databasedir + 'target') as afile:
        for line in afile:
            targetlist.append()

    return targetlist

#=================#
# GLOBAL SETTINGS #
#=================#
#top = '/home/timothy/Documents/sample'
#top = '/home/timothy/Documents'
#top = './mnt/sbin'
top = '/home/lam/Documents'
targets = findtarget()
databasedir = './databases/'
timeformat = "%d-%m-%Y %H:%M:%S" #DD-MM-YYYY hh:mm:ss
logdb = 'logs.db'
mountdir = './mnt'
expectedfs = ['ext2', 'ext3', 'ext4', 'NTFS', 'exFAT', 'FAT32']
ipfile = databasedir + 'slaves'

#=================#
# AV FILES CONFIG #
#=================#
cvd = "./cvd/main.cvd"
mdb = "./cvd/main.mdb"
activedb = databasedir + 'virus.db'
database = [activedb, activedb+'.old', activedb+'.older', activedb+'.oldest']
files_needed = ["main.info", "main.mdb"]

#==================#
# GLOBAL VARIABLES #
#==================#
filelist = mp.Queue()
queue_end = "4F4C835AD3B80D56B3C05935612D37A01B98F8BAE6A79BA02C6FD96B298FD2EE7D55DBD7CE194C0713A26239F05E580C41EFC95FE077B2E4B372C1770E3DFE1F"
mountqueue = {}
sendfiles = ['/databases/virus.db', '/databases/targets.txt']
