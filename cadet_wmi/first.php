<?php
require_once('./header.php');

if (!is_dir('./resources/databases/')) {
    mkdir('./resources/databases/');
    $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator('./resources'));

    foreach($iterator as $item) {
        chmod($item, 777);
        chown($item, 'pi');
    }
}

$firstdb = new SQLite3('./resources/databases/cadet_wmi.db');
if(!$firstdb) {
    echo $firstdb->lastErrorMsg();
}

if(!is_file('./resources/databases/logs.db')) {
    system('ln -s /home/timothy/Documents/MP/CADET/databases/logs.db /home/timothy/Documents/MP/CADET/cadet_wmi/resources/databases/logs.db');
}

$firstdb->exec('CREATE TABLE users(username STRING UNIQUE NOT NULL, password STRING NOT NULL, restrictions INT NOT NULL, date STRING NOT NULL, token STRING NOT NULL)');

$hash = password_hash("password", PASSWORD_BCRYPT);
$date = date("d-m-Y H:i:s");
$firstdb->exec('INSERT INTO users(username, password, restrictions, date, token) VALUES ("admin", "'.$hash.'", 0, "'.$date.'", "")');
