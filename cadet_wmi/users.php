<?php
require_once('./conn.php');
require_once('./header.php');

echo "<head>";
echo "<title>USER MGMT | CADET WMI</title>";
echo "<link rel='stylesheet' href='styles.css'>";
echo "</head>";

echo "<body>";
echo "<center>";
echo "<div vertical-align='middle' text-align='center'>";
    echo "<a href='./home.php'><img src='./resources/images/logo200.png' /></a>";
    echo "<div class='ribbon'>User Management</div>";
    echo "<div class='ribbon'><a href='./quarantine.php'>Quarantined Files</a></div>";
    echo "<div class='ribbon'><a href='./logs.php'>View Logs</a></div>";
    echo "<div class='ribbon'><a href='./config.php'>Configure CADET</a></div>";
    echo "<div id='logout'><a href='./logout.php'>Logout ".$_SESSION['uname']."</a></div>";
echo "</div><br>";

if(isset($_GET['task']) && isset($_GET['uname']) && isset($_GET['token'])) {
    require_once('./conn.php');
    $result = $db->query('SELECT token,restrictions FROM users WHERE username = "'.$_GET['uname'].'"');
    $row = $result->fetchArray();

    if ($_GET['token'] != $_SESSION['token']) {
        require_once('./destroysession.php');
    }

    if ($_GET['task'] == "edit") {
        $uname = $_GET['uname'];

        echo "Editing information for user <b>".$uname."</b><br><br>";

        echo "<a href='./users.php'><input type='button' value='&lt;&lt;Go Back'></a><br><br>";

        echo "<form action='./modifyuser.php' method='post'>";
        echo "Username<br>";

        if ($uname == 'admin') {
            echo "<input type='text' size='20' class='textbox' value='".$uname."' disabled><br><br>";
        }

        else {
            echo "<input type='text' size='20' class='textbox' value='".$uname."' disabled><br><br>";
        }

        echo "Current Password (required for any changes)<br>";
        echo "<input type='password' class='textbox' placeholder='PASSWORD' pattern='[A-Za-z0-9,.+\-_]{6,12}' name='pword' required><br><br>";
        if (isset($errorpword)) { echo $errorpword; }

        echo "New Password. Leave blank if not changing<br>";
        echo "<input type='password' class='textbox' placeholder='PASSWORD' pattern='[A-Za-z0-9,.+\-_]{6,12}' name='newpword'><br><br>";

        echo "Retype New Password. Leave blank if not changing<br>";
        echo "<input type='password' class='textbox' placeholder='RETYPE PASSWORD' pattern='[A-Za-z0-9,.+\-_]{6,12} name='newrepword'><br><br>";
        if (isset($errornewpword)) { echo $errornewpword; }

        echo "Restriction Level<br>";

        if ($uname == 'admin') {
            echo "<select name='restriction' disabled>";
        }

        else {
            echo "<select name='restriction'>";
        }
            echo "<option value='-1' selected>Unchanged</option>";
            echo "<option value='0'>0 - Administrative</option>";
            echo "<option value='1'>1</option>";
            echo "<option value='2'>2 - View only</option>";
        echo "</select><br><br>";

        echo "<button type='reset'>Reset Form</button><br><br>";
        echo "<input type='submit' value='Submit Form' name='edit'>";
        echo "</form>";
    }

    else if ($_GET['task'] == "delete") {
        $uname = $_GET['uname'];
        echo "Deleting user account <b>".$uname."</b><br><br>";
        echo "<a href='./users.php'><input type='button' value='&lt;&lt;Go Back'></a><br><br>";

        if ($_SESSION['restrictions'] != 0) {
            echo "You need to be an administrator to delete user accounts!";
        }

        else if (($_SESSION['restrictions'] == 0 || $_SESSION['username'] == $_GET['uname']) && $uname != 'admin') {
            echo "Are you sure you want to delete this account?";

            echo "<form action='./modifyuser.php' method='post'>";
            echo "Check the box below to confirm that you are deleting the user account <b>".$uname."</b><br><br>";
            echo "<input type='checkbox' name='confirm' value='Confirm' required><br><br>";
            echo "<input type='submit' name='delete' value='Delete'>";
        }
    }
}

else {

    echo "<form action='./modifyuser.php' method='post'>";
    echo "<input type='submit' name='create' value='Create New User'>";
    echo "</form>";

    $results = $db->query('SELECT username,restrictions,date FROM users');

    echo "To change user account password or restriction level, click on \"Edit\".<br>";
    echo "User <b>admin</b>'s username and restriction level <b>CANNOT</b> be changed.<br><br>";
    echo "<table width='650px'>";
    echo "<th width='170px'>Username";
    echo "<th width='120px'>Restriction Level";
    echo "<th width='170px'>Date Created";
    echo "<th width='45px'>Edit";
    echo "<th width='45px'>Delete";

    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
        $uname = $row['username'];
        $restrictions = $row['restrictions'];
        $date = $row['date'];

        echo "<tr>";
            echo "<td>".$uname;
            echo "<td>".$restrictions;
            echo "<td>".$date;
            echo "<td width='45px'><a href='./users.php?task=edit&uname=".$uname."&token=".$_SESSION['token']."'>Edit</a>";
            if ($uname != 'admin') {
                echo "<td width='45px'><a href='./users.php?task=delete&uname=".$uname."&token=".$_SESSION['token']."'>Delete</a>";
            }
            else {
                echo "<td width='45px'>";
            }
        echo "</tr>";
    }
}
