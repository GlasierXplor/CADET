import os
import conf
import logger

def walker():
    top = conf.top
    filelist = conf.filelist

    while not filelist.empty():
        filelist.get_nowait()

    logger.logger("INFO", "Pushing files into queue...", "scan");
    print 'Pushing files into queue'
    try:
        for root, dirs, files in os.walk(top):
            if not root == './mnt/dev':
                for f in files:
                    path = os.path.join(root,f)
                    if not os.path.islink(path):
                        filelist.put(path)
    except os.error as e:
        logger.logger("ERROR", e, "scan")

    filelist.put(conf.queue_end)
    logger.logger("INFO", "Number of files: " + str(filelist.qsize()), "scan ")
