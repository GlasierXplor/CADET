=================
    Changelog
=================
4-6-2018
- Updated README.md.
- Included LICENSE.
- Changed "dependencies_server.sh" to dependencies_master.sh.
- Added "dependencies_slave.sh" to help ease installation of CADET slaves.
- Included the final report that has a detailed explanation of how CADET works.

4-6-2018
- Updated README.md.
- Added "dependencies_server.sh" to help ease installation of CADET master.

2-3-2018
- Project will be frozen to focus on completing the report. Further improvements may be made after submission of final report.
- There is a bug where the final file will NOT be completed scanning. Trying to find the cause of this bug.

27-2-2018
- Added distribute.py, a file that will contain all system calls to send commands over ssh by using pssh (PARALLEL SSH).
- iSCSI bug has been fixed, but found another small bug - it will not check whether any disks are being mounted on the mountpoint at the moment. Will try to fix this.
- The whole program has been tested to work successfully on a local machine, with an iSCSI server with multiple disks
- Fixed a bug that will cause the whole computer to freeze if hashing a file it doesn't understand.
    + /dev folder has been excluded as this is the folder that causes the problem.

23-2-2018
- Added a line of code to optimise database. SELECT Statements now run 1000x faster on 8 cores (literally)
    + Depending on file size, it can range from 8 files/10 seconds (200MB), to 40,000 files/10 seconds (Smaller system files)

21-2-2018
- iSCSI implementation is complete with a small bug. Will try to fix it.
- iSCSI disks can be mounted and scanned.
- Scanning speed is at 4-5 files/10 seconds on 8 cores (i7-8550U).
	+ Largest bottleneck being 'SELECT' statements in SQLite.

17-2-2018
- User Management is almost complete.
- Started work on displaying logs in WMI.

16-2-2018
- Session management implemented.
- Started work on user management page.
- Logging is now in working condition.
- Updated Readme.md

15-2-2018
- Started work on a Web Management Interface for CADET, under the folder cadet_wmi/
- Login page is in working condition.

14-2-2018
- CADET has migrated from using the Epiphany core to using multiprocessing using the cores on a Raspberry Pi.
- Quarantine is now working - files will be put into a quarantine directory with a CQFF file header.
- Dequarantine is now working - file metadata will be altered upon returning the file to its original location.
- Reorganised conf.py to make sure that everything is categorised.
- Changed Queue.Queue. to multiprocessing.Queue for better compatibility with multiprocessing codes.
- The program is now running in true multiprocessing, with significantly better results.

6-2-2018
- Versioning of databases is now in full working condition.

1-2-2018
- Troubleshooting versioning -- no old databases are versioned.

29-1-2018
- Added database versioning - CADET will keep the last three successful databases that can be used as fallbacks when necessary.
- Working on picking the right fallback database to use.

27-1-2018
- Added requirements.txt containing a list of Python dependencies for CADET to run properly.
- Added quarantine.py - started work on quarantining suspected infected files.

26-1-2018
- scan.py is now in working condition.
- Hashes + filesize comparison implemented.
    = Proof: empty files produces the same hashes as a worm - alert was raised but not as a confirmed alert as filesize was mismatched.

20-1-2018
- walker.py uses qsize() instead of using "for, x += 1".
- walker.py done. Pushes ~470000 files in under 5 seconds.

19-1-2018
- database.py will now delete the old database and create a new one in the case where an update has been issued.
- update.py has been updated to correct some mistakes.
- Started work on walker.py that will push all file paths into a queue for scanning purposes.
- Removed checkdb.py due to the fact that SQLite is flexible like that -- delete the database and create a new one (either way it takes 3 - 4 seconds to create a new database)

16-1-2018
- update.py has a new mode "FORCE" that will download and overwrite the current version. In case the current version may be corrupted.
- Implemented individual extracted file verification.
- Database will now be populated in the case where it is missing, but CVD for some reason is still present - in the case where the database may be accidentally deleted by users/sysadmins.
- Database can now be updated with new signatures. Will research if the old ones will be removed or not. If it will be removed, it may prove to be a problem as the unique keys may be messed up.

15-1-2018
- update.py will now only download file headers if the command issued is update. It will then download the entire cvd only if the hash is different.

14-1-2018
- Updated all print commands to include Categories of Error to ease transitions when logging will be implemented
- database.py will now create a new virus.db if an existing one cannot be found (if files needed are present but for some reason the database is not)

13-1-2018
- Streamlined update.py to accomodate for more situations
- Defined Categories of Error to help ease transition to implement logging
- database.py can now create a new virus.db and insert all data within main.mdb into the database (new download + first population of db)

12-1-2018
- Created repository structure
- update.py will fetch main.cvd from clamav.net and will automatically unpack. It will then either update the current main.mdb or just leave it there as a new download.
- update.py will show a progress statement when downloading main.cvd
