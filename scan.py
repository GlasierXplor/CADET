import conf
import database
import hashlib
import os
import quarantine
import multiprocessing as mp

def hasher(filename):
    BLOCKSIZE = 65536
    hasher = hashlib.md5()
    try:
        afile = open(filename, 'rb')
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
        md5hash = hasher.hexdigest()
        afile.close()
        return md5hash

    except:
        pass

def scan(queuelock, printlock):
    filelist = conf.filelist

    def printer(returned, index, filename):
        with printlock:
            print "WARN: Virus Type: " + returned[index][2]
            print "WARN: Virus Hash: " + returned[index][1]
            print "WARN: File Path : " + filename


    print "INFO: Scanning started."

    while True:
        with queuelock:
            filename = filelist.get()

        #with printlock:
        print filename

        if filename == conf.queue_end:
            with queuelock:
                filelist.put(conf.queue_end)
                print "INFO: Done"
            break
            #pass control back to main.py

        else:
            md5hash = hasher(filename)

            database.conn(conf.activedb)
            cursor = database.cursor
            cursor.execute('SELECT * FROM virushash WHERE hash = ?', (md5hash,))
            returned = cursor.fetchall()

            try:
                filesize = os.path.getsize(filename)
            except:
                pass

            if (len(returned) != 0) and (filesize > 0): #Technically no point matching empty files
                printlock.acquire()
                print "WARN: Virus hash match found! Matching filesize..."
                upper = []
                lower = []

                for item in returned:
                    upper.append(item[0] + (item[0] * 10/100))
                    lower.append(item[0] + (item[0] * 10/100))

                for index in range(0,len(returned)-1):
                    if filesize == returned[index][0]:
                        print "WARN: Exact match found."
                        printer(returned, index, filename)
                        #quarantine.quarantine(filename, md5hash, filesize)

                    elif lower[index] <= filesize <= upper[index]:
                        print "WARN: No exact match, but filesize is within 10% of exact match."
                        printer(returned, index, filename)
                        #quarantine.quarantine(filename, md5hash, filesize)

                    else:
                        print "WARN: Hash values matches, but file size doesn't - potential false alarm."
                        printer(returned, index, filename)
