#!/bin/bash

$package = "pssh python-mpi4py python-sqlite openssh-client open-iscsi nginx php php-fpm php-sqlite3"

#Check root user/sudo
if [ "$EUID" -ne 0 ]; then
    echo -e "\e[41mPlease run script as root!\e[0m"
    exit
fi

#Check internet connection
wget -q --tries=10 --timeout=20 -O - http://google.com > /dev/null
if [[ $? -eq 0 ]]; then
    echo "Internet connection detected!"
else
    echo -e "\e[41mNo internet connection! Please check your network connectivity\e[0m"
    exit
fi

#Check if apt-get is present
which apt-get > /dev/null
if [[ $? -ne 0 ]]; then
    echo -e "\e[41mScript can only be run on systems derived from \e[0m"
    exit
fi

#Check if for some reason Python is missing
which python > /dev/null
if [[ $? -ne 0 ]]; then
    $package = "python $package"
fi

sudo apt-get -y install $package
if [[ $? -eq 0 ]]; then
    echo -e "\e[42mDone. Be sure to run dependencies_clients.sh on slave machines\e[0m"
fi
