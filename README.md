# The Cloud Anti-malware &amp; DETector
CADET is a malware scanner optimised for a Raspberry Pi cluster. It can run perfectly fine with one Raspberry Pi, and can be scaled up to multiple Pis in a Pi cluster.

It can run on virutally any machines that are running an Ubuntu or Debian derived distros.

## Minimum Requirements
- Ubuntu 16.04 LTS Xenial Xerus or Debian 9 Stretch or their derivatives
    - No GUI - resources are scarce in a bare minimum setup
- 1GB RAM
    - Python running CADET's main code
    - Nginx and php running the WMI
    - SQLite managing the databases
- Multi-core CPUs preferred
    - Tests have shown that a single core will run at about ~1.4GHz max. So any modern CPUs should provide enough computational power to CADET.
- 5GB free hard disk space
    - ~2GB:  CADET files and dependencies
    - 700MB: ClamAV Virus Database
    - 300MB: SQLite Databases
    - Logs

## Recommended Minimum Requirements
- Ubuntu 16.04 LTS or its derivatives preferred
    - No GUI preferred - give as much resources to CADET as possible
- 2GB RAM
    - 1GB extra for everything to play nicely
- Multi-core CPUs, with at least 2.0GHz clock speed
- 10GB free hard disk space
    - More space for logs = less deletion = more trails

## Dependencies
### Python
- pssh (Only on master node)
- mpi4py

### Linux
- SSH
    - Master node needs an SSH client
    - Slave nodes need SSH servers
- OpenMPI (Should be installed along when you install mpi4py)
- open-iscsi

### LESP Stack (Only On Master Node)
- nginx
- php
- php-sqlite3

### **Important:
- All Computers running in the same cluster **MUST** use the same version of OpenMPI.

## Installing CADET Dependencies

The dependencies can be installed by running the script "dependencies_master.sh" on the master node and "dependencies_slave.sh" on the slave nodes. The script can only be run on Debian and Ubuntu derived distros.

    //on master node
    chmod +x dependencies_master.sh
    sudo ./dependencies_master.sh

    //on slave nodes
    chmod +x dependencies_slave.sh
    sudo ./dependencies_slave.sh

## Preparing CADET for Deployment
1. You will need to create the following text files in a new directory './databases/' on the master node:
    ```
    ./database/slaves
      - This file will contain a list of IP addresses of all
        slave machines in the same computing cluster, one on each line.

    ./database/hosts
      - This file will contain a list of IP addresses of all
        iSCSI targets in the network, one on each line.
    ```

2. You will need to ensure that **ALL** machines in the cluster can communicate with the iSCSI server.

## Deploying CADET
To run CADET, simply change your directory to CADET and run

    sudo python main.py

## Test Bench
During development CADET was run on my own laptop. The following are the specifications of my laptop:
- CPU: Intel 8<sup>th</sup> Generation i7-8550U
- CPU core count: 8 cores
- CPU clock speed: 1.8GHz minimum, turbo boost up to 4.0GHz
- RAM: 16GB
- OS: elementary OS 0.4.1 Loki 64-bit (With GUI), built on Ubuntu 16.04.3 LTS Xenial Xerus

During development CADET was also tested on a Raspberry Pi 3 Model B. The following are the specifications:
- CPU: Broadcomm BCM2837
- CPU core count: 4 cores
- CPU clock speed: 1.2GHz, online tests showing that it can go up to 1.4GHz without problems
- RAM: 1GB
- OS: Raspbian Stretch Lite (No GUI), built on Debian Stretch

### Note:
CADET was not able to run with the Raspberry Pi as a slave node as both machines have different versions of OpenMPI in their respective repositories and thus are not able to communicate with one another.
