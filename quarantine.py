import datetime
import conf
import os
import hashlib

def header(filepath, filehash, filesize, quatime):
    magicnumber = "CQFF" #43 51 46 46 CADET Quarantine File Format
    headerstring = magicnumber + ";" + str(filesize) + ";" + filepath + ";" + filehash + ";" + quatime
    while len(headerstring) < 512:
        headerstring = headerstring + "\x00"
    return headerstring

def quarantine(filepath, filehash, filesize):
    #quadir = os.path.join(os.path.abspath + "cadet_quarantine")
    quadir = "/home/timothy/Documents/MP/CADET/cadet_quarantine"
    basedir, filename = os.path.split(filepath)
    if not os.path.isdir(quadir):
        try:
            os.mkdir(quadir)
        except:
            print "FATAL: Cannot create quarantine directory! Check directory permissions."
            exit()

    timenow = datetime.datetime.now().strftime(conf.timeformat)
    fileheader = header(filepath, filehash, filesize, timenow)

    with open(filepath, 'rb') as srcfile:
        quafile = os.path.join(quadir, filename + ".cqff")
        with open(quafile, 'ab') as dstfile:
            dstfile.write(fileheader)
            dstfile.write(srcfile.read())
    os.remove(filepath)
    print "INFO: " + filename + " quarantined!"

def dequarantine(filename):
    cqfffile = "./cadet_quarantine/" + filename + ".cqff"
    with open(cqfffile, 'rb') as quafile:
        fileheader = quafile.read()[0:512]
        magicnumber, filesize, filepath, filehash, quatime = fileheader.split(';')
        if os.path.isfile(filepath):
            print "FATAL: Cannot restore file. A file with the same name exists in the same directory."
        else:
            with open(filepath, 'wb') as dstfile:
                quafile.seek(0)
                dstfile.write(quafile.read()[512:])

            BLOCKSIZE = 65536
            hasher = hashlib.md5()
            with open(filepath, 'rb') as dstfile:
                buf = dstfile.read(BLOCKSIZE)
                while len(buf) > 0:
                    hasher.update(buf)
                    buf = dstfile.read(BLOCKSIZE)
            md5hash = hasher.hexdigest()
            if md5hash == filehash:
                print "INFO: Successfully restored " + filename
                os.remove(cqfffile)
            else:
                print "ERROR: Hash value mismatch"
