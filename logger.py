import datetime
import conf
import sqlite3 as sql
import os

def initialise():
    if not os.path.isdir(conf.databasedir):
        os.makedirs(conf.databasedir)

    dbfile = conf.databasedir + conf.logdb

    global logdb
    logdb = sql.connect(dbfile)

    tables = ['database', 'scan', 'quarantine']

    for table in tables:
        global cursor
        cursor = logdb.cursor()
        cursor.execute('CREATE TABLE IF NOT EXISTS ' + table + '(datetime STRING NOT NULL, severity STRING NOT NULL, log STRING NOT NULL)')
        logdb.commit()

def logger(severity, log, source):
    timenow = datetime.datetime.now().strftime(conf.timeformat)
    inputstring = (timenow, severity, log)
    cursor.execute('INSERT INTO ' + source + ' VALUES(?,?,?)', inputstring)
    logdb.commit()
