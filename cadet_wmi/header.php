<?php
require_once('./errorheader.php');
session_start();

if (!isset($_SESSION['uname']) || !isset($_SESSION['token']) || !isset($_SESSION['restrictions'])) {
    require_once('./destroysession.php');
}

else {
    require_once('./conn.php');
    $result = $db->query('SELECT token, restrictions FROM users where username = "'.$_SESSION['uname'].'"');
    $row = $result->fetchArray();
    if(empty($row)) {
        require_once('./destroysession.php');
    }
    else if (!empty($row)) {
        if($_SESSION['token'] != $row['token']) {
            require_once('./destroysession.php');
        }
    }
}
?>
