import database
import walker
import scan
import multiprocessing as mp
import quarantine
import logger
import iscsi
import conf
import time
#import distribute

if __name__ == '__main__':
    #initialises all log databases.
    logger.initialise()

    #Fetches and/or updates the database. Makes sure
    #The database is in a good condition to be used.
    database.checkfiles()

"""
    #Establish connection to iSCSI server
    iscsi.findiscsi()

    distribute.files()

#    distribute.connectiscsi()
    iscsi.connectiscsi()

    #distribute.findluns()
    iscsi.findluns()

    for target in conf.mountqueue:
        device = conf.mountqueue[target]
        #Mounts a drive
        iscsi.mountluns(device)

        #Pushes files into a queue
        walker.walker()

        print str(conf.filelist.qsize())

        queuelock = mp.Lock()
        printlock = mp.Lock()
        jobs = []

        for i in range(0,mp.cpu_count()):
            process = mp.Process(target=scan.scan, args=(queuelock,printlock))
            jobs.append(process)
            #Start the scanning processes
            process.start()

        for x in jobs:
            x.join()

        #unmount drive
        iscsi.unmountluns()
        logger.logger("INFO", "Scanning ended", "scan")
"""

walker.walker()

queuelock = mp.Lock()
printlock = mp.Lock()
jobs = []

for i in range(0, mp.cpu_count()):
    process = mp.Process(target=scan.scan, args = (queuelock, printlock))
    jobs.append(process)
    process.start

    for x in jobs:
        x.join()