<?php
require_once('./errorheader.php');
session_start();

if(!empty($_SESSION['uname'])) {
    header('location:./home.php');
}

$error = 0;

if (isset($_POST['login'])) {
    $uname = $_POST['uname'];
    $pword = $_POST['pword'];

    if(empty($uname)) {
        $erroruname = "Please enter a username!";
        $error = $error + 1;
    }

    if(!empty($uname)) {
        if(!preg_match("/^[A-Za-z0-9,.+\-_]{4,10}$/", $uname) || strpos($uname, "--") !== false) {
            if(strlen($uname) < 4) {
                $erroruname = "Invalid length!";
                $error = $error + 1;
            }
            else {
                $erroruname = "Invalid characters!";
                $error = $error + 1;
            }
        }
    }

    if(empty($pword)) {
        $errorpword = "Please enter a password!";
        $error = $error + 1;
    }

    if(!empty($pword)) {
        if(!preg_match("/^[A-Za-z0-9,.+\-_]{6,12}$/", $pword) || strpos($pword, "--") !== false) {
            if(strlen($pword) < 6) {
                $errorpword = "Invalid length!";
                $error = $error + 1;
            }
            else {
                $errorpword = "Invalid characters!";
                $error = $error + 1;
            }
        }
    }

    if($error == 0) {
        require_once('./conn.php');
        $results = $db->query('SELECT username,password,restrictions FROM users WHERE username = "'.$uname.'"');
        $row = $results->fetchArray();

        if(!empty($row)) {
            $checkpword = $row['password'];
            if(password_verify($pword, $checkpword)) {
                $secret = uniqid('', true).$uname;
                $token = hash('sha256', $secret);
                $db->exec('UPDATE users SET token = "'.$token.'" WHERE username = "'.$uname.'"');
                $_SESSION['uname'] = $uname;
                $_SESSION['token'] = $token;
                $_SESSION['restrictions'] = $row['restrictions'];
                header('location:./home.php');
            }

            else {
                $error = $error + 1;
                $errorgeneric = "Username or Password incorrect!";
            }
        }

        else {
            $error = $error + 1;
            $errorgeneric = "Username or Password incorrect!";
        }
    }
}

echo "<head>";
echo "<title>CADET WMI Login</title>";
echo "<link rel='stylesheet' href='styles.css'>";
echo "</head>";

echo "<body>";
echo "<center>";
echo "<a href='./index.php'><img id='loginlogo' src='./resources/images/logo450.png' /></a>";
echo "<br>";

echo "<b><h2>Cloud Anti-malware & DETector</h2>";
echo "<h3>Web Management Interface Login</b></h3>";

echo "<form action='./login.php' method='post'>";

$uname = (!empty($_POST['uname']) ? $_POST['uname'] : "");
echo "<input type='text' placeholder='USERNAME' name='uname' value='".$uname."' title='Between 4 and 10 characters.' size='20' class='textbox' pattern='[A-Za-z0-9,.+\-_]{4,10}' autofocus required>";

$pword = (!empty($_POST['pword']) ? $_POST['pword'] : "");
echo "<input type='password' placeholder='PASSWORD' name='pword' value='".$pword."' size='20' class='textbox' pattern='[A-Za-z0-9,.+\-_]{6,12}' required><br>";

if($error !== 0) {
    if(!empty($erroruname)) {
        echo "<p class='error'>Error in Username: ".$erroruname."<br>";
    }
    if(!empty($errorpword)) {
        echo "<p class='error'>Error in Password: ".$errorpword."<br>";
    }
    if(!empty($errorgeneric)) {
        echo "<p class='error'>Error: ".$errorgeneric."<br>";
    }
}

echo "<br>";
echo "<input type='submit' value='LOG IN!' id='login' name='login'>";
echo "</center>";
echo "</body>";
?>
