<?php
require_once('./header.php');

if (!empty($_SESSION['uname']) || !empty($_SESSION['token']) || !empty($_SESSION['restrictions'])) {
    require_once('./conn.php');
    $db->exec('UPDATE users SET token="" WHERE username="'.$_SESSION['uname'].'"');
    session_unset();
    session_destroy();
}
header('location:./login.php');
?>
