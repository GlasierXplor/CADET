<?php
require_once('./header.php');

class newdb extends SQLite3
{
    function __construct()
    {
        if (!is_link('./resources/databases/logs.db')) {
            system('ln -s /home/timothy/Documents/MP/CADET/databases/logs.db /home/timothy/Documents/MP/CADET/cadet_wmi/resources/databases/logs.db');
        }
        $this->open('./resources/databases/logs.db');
    }
}

$logdb = new newdb();
if(!$logdb) {
    echo $logdb->lastErrorMsg();
}

if (!isset($_GET['l'])) {
    header('location:./logs.php?l=overview');
}

if(isset($_GET['l'])) {
    $link = $_GET['l'];
    if ($link == 'overview') {
        $output =
        "<table id='logstatus'>
        <th>Logs Category</th>
        <th>Logs Status</th>
        <th>Clear Logs</th>

        <tr>
        <td>Database</td>
        <td>Good</td>
        <td><input type='submit' value='Clear Log' name='cleardb'></td>
        </tr>

        <tr>
        <td>Scan</td>
        <td>Good</td>
        <td><input type='submit' value='Clear Log' name='clearsc'></td>
        </tr>

        <tr>
        <td>Quarantine</td>
        <td>Good</td>
        <td><input type='submit' value='Clear Log' name='clearqua '></td>
        </tr>
        ";
    }

    else if ($link == 'database') {
        $query = 'SELECT datetime,log,severity FROM database';
        $results = $db->query($query);
        $row = $results->fetchArray();

        if (empty($row)) {
            $output = 'No Logs to display!';
        }

        else {
            $output =
            "<table id=logstatus'>
            <th>Datetime</th>
            <th>Logs</th>
            <th>Severtiy</th>

            <tr>
            <td>
            ";
        }
    }

    else if ($link == 'scan') {
    }

    else if ($link == 'quarantine') {
    }
}

echo "<head>";
echo "<title>LOGS | CADET WMI</title>";
echo "<link rel='stylesheet' href='styles.css'>";
echo "</head>";

echo "<body>";
echo "<center>";
echo "<div vertical-align='middle' text-align='center'>";
    echo "<a href='./home.php'><img src='./resources/images/logo200.png' /></a>";
    echo "<div class='ribbon'><a href='./users.php'>User Management</a></div>";
    echo "<div class='ribbon'><a href='./quarantine.php'>Quarantined Files</a></div>";
    echo "<div class='ribbon'>View Logs</div>";
    echo "<div class='ribbon'><a href='./config.php'>Configure CADET</a></div>";
    echo "<div id='logout'><a href='./logout.php'>Logout ".$_SESSION['uname']."</a></div>";
echo "</div><br>";

echo "<div class='tab'>";
    echo "<a href='./logs.php?l=overview'><div class='tablinks'>Overview</div></a>";
    echo "<a href='./logs.php?l=database'><div class='tablinks'>Database</div></a>";
    echo "<a href='./logs.php?l=scan'><div class='tablinks'>Scan</div></a>";
    echo "<a href='./logs.php?l=quarantine'><div class='tablinks'>Quarantine</div></a>";
echo "</div>";

echo "<div id='content'>";
    echo $output;
echo "</div>";
?>
