import subprocess
import os
import logger
import conf

def findiscsi():
    #Cannot find a more Pythonic way, so have to use system calls
    targets = conf.targets
    tmptgtfile = conf.databasedir + '/tmptargets.txt'
    tgtfile = conf.databasedir + '/targets.txt'
    for target in targets:
        try:
            subprocess.check_output('sudo iscsiadm --mode discovery -t sendtargets --portal ' + target + ' > ' + tmptgtfile, shell=True)

        except subprocess.CalledProcessError as e:
            print "FATAL: Cannot connect to iSCSI target! Check configuration. Error code: " + str(e.returncode)
            os.remove(tmptgtfile)
            exit()

    with open(tmptgtfile, 'r') as temptarget:
        with open(tgtfile, 'w+') as targetfile:
            for line in temptarget:
                lineexists = False
                dirtyip, targetiqn = line.split(' ')
                ip, garbage = dirtyip.split(',')
                writer = ip + ';' + targetiqn

                for line in targetfile:
                    if writer == line:
                        lineexists = True

                if not lineexists:
                    targetfile.write(writer)

    os.remove(tmptgtfile)

def connectiscsi():
    tgtfile = conf.databasedir + '/targets.txt'
    targetlist = []
    try:
        #resets iscsiadm to no records state
        subprocess.check_output('sudo iscsiadm --mode node -u', shell=True)
#        subprocess.check_output('sudo iscsiadm --mode node -o delete', shell=True)

    except subprocess.CalledProcessError:
        pass

    with open(tgtfile, 'r') as targetfile:
        for line in targetfile:
            ip, targetiqn = line.split(';')
            targetiqn = targetiqn.replace('\n', '')
            targetlist.append([ip, targetiqn])

    try:
        for i in range(0,len(targetlist)):
            ip = targetlist[i][0]
            targetiqn = targetlist[i][1]
            #attempt to connect to every iscsi node in the file 'targets.txt'
            subprocess.check_output('sudo iscsiadm --mode node --targetname ' + targetiqn + ' --portal ' + ip + ' --login', shell=True)

    except subprocess.CalledProcessError as e:
        print "FATAL: Cannot connect to iSCSI target! Check configuration. Error code: " + str(e.returncode)
        exit()

def findluns():
    tmplunfile = conf.databasedir + '/tmpluns.txt'
    tmp2lunfile = conf.databasedir + 'tmp2luns.txt'
    lunfile = conf.databasedir + '/luns.txt'

    #assumes that connection to iSCSI server is already established
    try:
        subprocess.check_output('sudo iscsiadm --mode session -P 3 | grep "Target:\|sd" > ' + tmplunfile, shell=True)

    except subprocess.CalledProcessError as e:
        print "FATAL: Cannot connect to iSCSI target! Check configuration. Error code: " + str(e.returncode)
        os.remove(tmplunfile)
        exit()

    with open(tmplunfile, 'r') as tmpfile:
        with open(tmp2lunfile, 'w+') as tmp2file:
            for line in tmpfile:
                if line[0] == "T":
                    writer = line.replace('Target: ', '')

                    for num, char in enumerate(writer):
                        if char == "(":
                            pos1 = num
                        if char == ")":
                            pos2 = num

                    delete = ""
                    while pos1 <= pos2:
                        delete = delete + writer[pos1]
                        pos1 += 1

                    writer = writer.replace(delete, '')

                elif line[0] == "\t":
                    writer = line.replace('State: running', '')
                    writer = writer.replace('Attached scsi disk ', '/dev/')
                    writer = writer.replace('\t', '')

                tmp2file.write(writer)

    with open(tmp2lunfile, 'r') as tmpfile:
        with open(lunfile, 'w') as findlunfile:
            for line in tmpfile:
                writer = line.replace(' \n', ';')
                findlunfile.write(writer)

    with open(lunfile, 'r') as findlunfile:
        for line in findlunfile:
            target, disk = line.split(';')
            disk.replace('\n', '')
            conf.mountqueue[target] = disk

    print conf.mountqueue

    os.remove(tmplunfile)
    os.remove(tmp2lunfile)

def mountluns(device):
    if not os.path.isdir(conf.mountdir):
        os.mkdir(conf.mountdir)

    lunfile = conf.databasedir + '/luns.txt'
    tmpmntfile = conf.databasedir + '/tmpmount.txt'
    mntfile = conf.databasedir + '/mount.txt'

    expectedfs = ""
    for i in range(0,len(conf.expectedfs)):
        expectedfs = expectedfs + conf.expectedfs[i]
        if i != (len(conf.expectedfs) - 1):
            expectedfs = expectedfs + "\|"

    device = device.replace('\n', '')

    try:
        subprocess.check_output('sudo blkid -o list | grep "not mounted" | grep "' + expectedfs + '" | grep "' + device + '" > ' + tmpmntfile, shell=True)

    except subprocess.CalledProcessError as e:
        pass

    with open(tmpmntfile, 'r') as tmpfile:
        with open(mntfile, 'w') as mountfile:
            for line in tmpfile:
                field = line.split()
                mountfile.write(field[0])


    with open(mntfile, 'r') as mountfile:
        for line in mountfile:
            global drive
            drive = line.split(';')[0]

    os.remove(tmpmntfile)

    try:
        subprocess.check_output('sudo mount ' + drive + ' -o ro ' + conf.mountdir, shell=True)

    except subprocess.CalledProcessError as e:
        print 'ERROR mounting'
        exit()

def unmountluns():
    try:
        subprocess.check_output('sudo umount ./mnt', shell=True)

    except:
        pass
