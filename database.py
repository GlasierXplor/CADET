import sqlite3 as sql
import conf
import update
import os
import logger

#Connect to the SQLite database file
def conn(db):
    try:
        global virusdb
        virusdb = sql.connect(db)

    except:
        logger.logger("FATAL", "Cannot create/connect to " + db + ". Check folder permissions.", "database")
        exit()

    global cursor
    cursor = virusdb.cursor()
    cursor.execute('CREATE TABLE IF NOT EXISTS virushash (filesize INTEGER, hash TEXT, name TEXT)')
    virusdb.commit()

#Checking for databases present
def versioning():
    oldest = False
    allpresent = False
    for index in range(0,len(conf.database)):
        if not os.path.isfile(conf.database[index]):
            oldest = index
            break

        if index == len(conf.database) - 1:
            oldest = index
            allpresent = True

    if oldest is False and allpresent is False:
        conn(conf.activedb)
        createdb()

    else:
        if oldest == (len(conf.database) - 1) and allpresent is True:
            os.remove(conf.database[oldest])

        for i in reversed(range(0,index)):
            logger.logger("INFO", "Backing up pre-existing databases.", "database")
            os.rename(conf.database[i], conf.database[i+1])

        conn(conf.activedb)
        createdb()

#initialise and populate database given that it is empty
def createdb():
    errorcount = 0
    finalinsert = []

    with open("./cvd/main.mdb") as hashfile:
        logger.logger("INFO", "Populating database.", "database")
        count = 0

        for line in hashfile.readlines():
            line = line.rstrip("\n")
            data = line.split(':')
            insert = (data[0], data[1],data[2])
            finalinsert.append(insert)
            count += 1

    hashfile.close()
    cursor.executemany('INSERT INTO virushash VALUES (?,?,?)', finalinsert)
    virusdb.commit()

    while errorcount < 5:

        if errorcount == 4:
            logger.logger("FATAL", "Database cannot be initialised after 5 tries and no database to fallback on.", "database")
            exit()

        if cursor.rowcount == count:
            logger.logger("INFO", "Database initialised. " + str(cursor.rowcount) + " hashes added.", "database")
            cursor.execute('CREATE INDEX Idx1 ON virushash(hash)')
            break

        elif cursor.rowcount != count and cursor.rowcount > 0:
            logger.logger("ERROR", "Database not inserted properly. Retrying.", "database")
            os.remove(conf.activedb)
            createdb()
            errorcount += 1

        else:
            if errorcount < 4:
                logger.logger("ERROR", "Database cannot be initialised! Retrying.", "database")
                errorcount += 1

            else:
                if os.path.isfile('./virus.db.old') or os.path.isfile('./virus.db.older') or os.path.isfile('./virus.db.oldest'):
                    logger.logger("ERORR", "Database cannot be initialised after 5 tries! Finding database to fallback on.", "database")
                    fallbackdb()

                else:
                    logger.logger("FATAL", "Database cannot be initialised after 5 tries and no database to fallback on.", "database")
                    exit()

#ensures all database files are present
def checkfiles():
    #if files are not here
    if not os.path.isfile(conf.cvd):
        logger.logger("WARN", "main.cvd not found! Downloading.", "database")

        if not os.path.isdir("./cvd"):
            os.mkdir("./cvd")

        if update.download("DOWNLOAD"):
            #versions the databases and creates a new and updated one
            versioning()

        else:
            for files in conf.files_needed:
                if not os.path.isfile(files):
                    if found == 0:
                        logger.logger("FATAL", "" + files + " cannot be downloaded and there are no available databases to use.", "database")
                        exit()
                    else:
                        logger.logger("ERROR", "" + files + " cannot be downloaded, but there are old databases to use.", "database")

                else:
                    if found == 0:
                        logger.logger("ERROR", "Cannot download database, but " + files + " is present.", "database")
                        createdb()
                    else:
                        logger.logger("ERROR", "Cannot download database, but there are old databases that I can still use.", "database")

    #if main.cvd is present
    elif os.path.isfile(conf.cvd):
        logger.logger("INFO", "main.cvd found. Checking if verification file is present...", "database")

        if not os.path.isfile('./cvd/md5.hash'):
            logger.logger("WARN", "md5.hash not found - not trusting the current CVD. Downloading a new one.", "database")
            need_update = update.download("FORCE")

        else:
            logger.logger("INFO", "md5.hash found. Checking for updates.", "database")
            need_update = update.download("UPDATE")

        if need_update:
            logger.logger("INFO", "Updating database.", "database")
            versioning()

        elif not need_update:
            if not os.path.isfile(conf.activedb):
                logger.logger("WARN", "Virus Database not found. Creating a new one", "database")
                conn(conf.activedb)
                createdb()
            logger.logger("INFO", "Ready to perform scans.", "database")
            #return control to main.
